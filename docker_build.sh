#!/bin/bash

# Build app
gradle bootJar

# Build the docker image
docker build --tag cookbook/cookbook-backend:0.0.1 .
