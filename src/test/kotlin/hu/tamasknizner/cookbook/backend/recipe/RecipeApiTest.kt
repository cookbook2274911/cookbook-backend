package hu.tamasknizner.cookbook.backend.recipe

import hu.tamasknizner.cookbook.backend.IntegrationTestBase
import hu.tamasknizner.cookbook.backend.domain.NewIngredient
import hu.tamasknizner.cookbook.backend.domain.NewRecipe
import hu.tamasknizner.cookbook.backend.domain.NewRecipeIngredient
import hu.tamasknizner.cookbook.model.NewRecipeApiModel
import hu.tamasknizner.cookbook.model.NewRecipeIngredientApiModel
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

class RecipeApiTest : IntegrationTestBase() {

    @Test
    fun `find recipe by id should return recipe`() {
        val paradicsom = ingredientRepository.save(
            NewIngredient(
                name = "Paradicsom",
                description = "Friss paradicsom",
            )
        )

        val kolbasz = ingredientRepository.save(
            NewIngredient(
                name = "Kolbász",
                description = "Szaftos kolbász",
            )
        )

        val recipe = recipeRepository.save(
            NewRecipe(
                name = "Paradicsomos kolbász",
                description = "Paradicsomos kolbász leírása",
                ingredients = listOf(
                    NewRecipeIngredient(
                        ingredientId = paradicsom.id,
                        quantity = 1,
                    ),
                    NewRecipeIngredient(
                        ingredientId = kolbasz.id,
                        quantity = 2,
                    ),
                )
            )
        )

        mockMvc.get("/api/recipe/${recipe.id}").andExpect {
            status { isOk() }
        }.andExpect {
            content { contentType(MediaType.APPLICATION_JSON) }
            jsonPath("$.id") { value(recipe.id.toString()) }
            jsonPath("$.name") { value(recipe.name) }
            jsonPath("$.description") { value(recipe.description) }
            jsonPath("$.ingredients[0].ingredient.id") { value(kolbasz.id.toString()) }
            jsonPath("$.ingredients[0].ingredient.name") { value(kolbasz.name) }
            jsonPath("$.ingredients[0].ingredient.description") { value(kolbasz.description) }
            jsonPath("$.ingredients[0].quantity") { value(recipe.ingredients[0].quantity) }
            jsonPath("$.ingredients[1].ingredient.id") { value(paradicsom.id.toString()) }
            jsonPath("$.ingredients[1].ingredient.name") { value(paradicsom.name) }
            jsonPath("$.ingredients[1].ingredient.description") { value(paradicsom.description) }
            jsonPath("$.ingredients[1].quantity") { value(recipe.ingredients[1].quantity) }
        }
    }

    @Test
    fun `save recipe should return saved recipe`() {
        val paradicsomId = ingredientRepository.save(
            NewIngredient(
                name = "Paradicsom",
                description = "Friss paradicsom",
            )
        ).id

        val kolbaszId = ingredientRepository.save(
            NewIngredient(
                name = "Kolbász",
                description = "Szaftos kolbász",
            )
        ).id

        val recipe = NewRecipeApiModel(
            name = "Paradicsomos kolbász",
            description = "Paradicsomos kolbász leírása",
            ingredients = listOf(
                NewRecipeIngredientApiModel(
                    ingredientId = paradicsomId,
                    quantity = 1,
                ),
                NewRecipeIngredientApiModel(
                    ingredientId = kolbaszId,
                    quantity = 2,
                ),
            )
        )

        mockMvc.post("/api/recipe") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(recipe)
        }.andExpect {
            status { isOk() }
            content { contentType("application/json") }
        }
    }
}