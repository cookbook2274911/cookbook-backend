package hu.tamasknizner.cookbook.backend

import com.fasterxml.jackson.databind.ObjectMapper
import hu.tamasknizner.cookbook.backend.data.repository.IngredientRepository
import hu.tamasknizner.cookbook.backend.data.repository.RecipeRepository
import hu.tamasknizner.cookbook.backend.data.table.IngredientTable
import hu.tamasknizner.cookbook.backend.data.table.RecipeIngredientTable
import hu.tamasknizner.cookbook.backend.data.table.RecipeTable
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc

@SpringBootTest
@AutoConfigureMockMvc
class IntegrationTestBase {
    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    @Autowired
    lateinit var ingredientRepository: IngredientRepository

    @Autowired
    lateinit var recipeRepository: RecipeRepository

    @BeforeEach
    fun clearDatabase() {
        transaction {
            RecipeIngredientTable.deleteAll()
            RecipeTable.deleteAll()
            IngredientTable.deleteAll()
        }
    }
}