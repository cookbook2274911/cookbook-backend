package hu.tamasknizner.cookbook.backend.ingredient

import hu.tamasknizner.cookbook.backend.IntegrationTestBase
import hu.tamasknizner.cookbook.backend.domain.NewIngredient
import hu.tamasknizner.cookbook.model.NewIngredientApiModel
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

class IngredientApiTest : IntegrationTestBase() {

    @Test
    fun `find ingredient by id should return ingredient`() {
        val ingredient = ingredientRepository.save(
            NewIngredient(
                name = "Paradicsom",
                description = "Friss paradicsom",
            )
        )

        mockMvc.get("/api/ingredient/${ingredient.id}").andExpect {
            status { isOk() }
        }.andExpect {
            content { contentType(MediaType.APPLICATION_JSON) }
            jsonPath("$.id") { value(ingredient.id.toString()) }
            jsonPath("$.name") { value(ingredient.name) }
            jsonPath("$.description") { value(ingredient.description) }
        }
    }

    @Test
    fun `save ingredient should return saved ingredient`() {
        val ingredient = NewIngredientApiModel(
            name = "Paradicsom",
            description = "Friss paradicsom",
        )

        mockMvc.post("/api/ingredient") {
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(ingredient)
        }.andExpect {
            status { isOk() }
        }.andExpect {
            content { contentType(MediaType.APPLICATION_JSON) }
            jsonPath("$.id") { isNotEmpty() }
            jsonPath("$.name") { value(ingredient.name) }
            jsonPath("$.description") { value(ingredient.description) }
        }
    }
}