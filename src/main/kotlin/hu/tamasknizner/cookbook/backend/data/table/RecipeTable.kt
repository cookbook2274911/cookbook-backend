package hu.tamasknizner.cookbook.backend.data.table

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.UUID

object RecipeTable : UUIDTable("recipe") {
    val name = text("name")
    val description = text("description").nullable()
}

class RecipeEntity(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<RecipeEntity>(RecipeTable)

    var name by RecipeTable.name
    var description by RecipeTable.description
    val ingredients by RecipeIngredientEntity referrersOn RecipeIngredientTable.recipeId
}