package hu.tamasknizner.cookbook.backend.data.repository

import hu.tamasknizner.cookbook.backend.data.table.IngredientTable
import hu.tamasknizner.cookbook.backend.domain.Ingredient
import hu.tamasknizner.cookbook.backend.domain.NewIngredient
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Repository
class IngredientRepository {

    @Transactional
    fun save(newIngredient: NewIngredient): Ingredient {
        val savedIngredient = IngredientTable.insert {
            it[name] = newIngredient.name
            it[description] = newIngredient.description
        }
        return Ingredient(
            id = savedIngredient[IngredientTable.id].value,
            name = savedIngredient[IngredientTable.name],
            description = savedIngredient[IngredientTable.description],
        )
    }

    @Transactional(readOnly = true)
    fun findById(id: UUID): Ingredient? {
        return IngredientTable.select {
            IngredientTable.id eq id
        }.firstOrNull()?.let {
            Ingredient(
                id = it[IngredientTable.id].value,
                name = it[IngredientTable.name],
                description = it[IngredientTable.description],
            )
        }
    }

    @Transactional(readOnly = true)
    fun findAll(): List<Ingredient> {
        return IngredientTable.selectAll()
            .map {
                Ingredient(
                    id = it[IngredientTable.id].value,
                    name = it[IngredientTable.name],
                    description = it[IngredientTable.description],
                )
            }.sortedBy { it.name }

    }
}