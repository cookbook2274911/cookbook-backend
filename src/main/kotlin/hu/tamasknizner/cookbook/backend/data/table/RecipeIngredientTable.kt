package hu.tamasknizner.cookbook.backend.data.table

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.UUID

object RecipeIngredientTable : UUIDTable("recipe_ingredient") {
    val recipeId = reference("recipe_id", RecipeTable)
    val ingredientId = reference("ingredient_id", IngredientTable)
    val quantity = integer("quantity")
}

class RecipeIngredientEntity(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<RecipeIngredientEntity>(RecipeIngredientTable)

    var recipe by RecipeEntity referencedOn RecipeIngredientTable.recipeId
    var ingredient by IngredientEntity referencedOn RecipeIngredientTable.ingredientId
    var quantity by RecipeIngredientTable.quantity
}