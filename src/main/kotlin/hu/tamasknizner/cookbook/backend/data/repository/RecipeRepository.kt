package hu.tamasknizner.cookbook.backend.data.repository

import hu.tamasknizner.cookbook.backend.data.table.IngredientTable
import hu.tamasknizner.cookbook.backend.data.table.RecipeEntity
import hu.tamasknizner.cookbook.backend.data.table.RecipeIngredientTable
import hu.tamasknizner.cookbook.backend.data.table.RecipeTable
import hu.tamasknizner.cookbook.backend.domain.Ingredient
import hu.tamasknizner.cookbook.backend.domain.NewRecipe
import hu.tamasknizner.cookbook.backend.domain.Recipe
import hu.tamasknizner.cookbook.backend.domain.RecipeIngredient
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Repository
class RecipeRepository {

    @Transactional
    fun save(newRecipe: NewRecipe): Recipe {
        val savedRecipe = RecipeTable.insert {
            it[name] = newRecipe.name
            it[description] = newRecipe.description
        }
        newRecipe.ingredients.forEach { ingredient ->
            RecipeIngredientTable.insert {
                it[recipeId] = savedRecipe[RecipeTable.id]
                it[ingredientId] = ingredient.ingredientId
                it[quantity] = ingredient.quantity
            }
        }
        return findById(savedRecipe[RecipeTable.id].value) ?: error("Recipe not found after save")
    }

    @Transactional(readOnly = true)
    fun findById(id: UUID): Recipe? {
        return RecipeTable
            .join(RecipeIngredientTable, JoinType.INNER, RecipeTable.id, RecipeIngredientTable.recipeId)
            .join(IngredientTable, JoinType.INNER, RecipeIngredientTable.ingredientId, IngredientTable.id)
            .select { RecipeTable.id eq id }
            .distinctBy { it[RecipeTable.id] }
            .singleOrNull()?.let {
                RecipeEntity.wrapRow(it).toDomain()
            }
    }

    @Transactional(readOnly = true)
    fun findAll(): List<Recipe> {
        return RecipeTable
            .join(RecipeIngredientTable, JoinType.INNER, RecipeTable.id, RecipeIngredientTable.recipeId)
            .join(IngredientTable, JoinType.INNER, RecipeIngredientTable.ingredientId, IngredientTable.id)
            .selectAll()
            .distinctBy { it[RecipeTable.id] }
            .map {
                RecipeEntity.wrapRow(it).toDomain()
            }
    }
}

private fun RecipeEntity.toDomain(): Recipe =
    Recipe(
        id = id.value,
        name = name,
        description = description,
        ingredients = ingredients.map { recipeIngredient ->
            RecipeIngredient(
                ingredient = Ingredient(
                    id = recipeIngredient.ingredient.id.value,
                    name = recipeIngredient.ingredient.name,
                    description = recipeIngredient.ingredient.description,
                ),
                quantity = recipeIngredient.quantity,
            )
        }.sortedBy { it.ingredient.name }
    )