package hu.tamasknizner.cookbook.backend.data.table

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.UUID

object IngredientTable : UUIDTable("ingredient") {
    val name = text("name")
    val description = text("description").nullable()
}

class IngredientEntity(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<IngredientEntity>(IngredientTable)

    var name by IngredientTable.name
    var description by IngredientTable.description
}