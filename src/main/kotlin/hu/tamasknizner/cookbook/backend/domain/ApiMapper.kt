package hu.tamasknizner.cookbook.backend.domain

import hu.tamasknizner.cookbook.model.IngredientApiModel
import hu.tamasknizner.cookbook.model.NewIngredientApiModel
import hu.tamasknizner.cookbook.model.NewRecipeApiModel
import hu.tamasknizner.cookbook.model.NewRecipeIngredientApiModel
import hu.tamasknizner.cookbook.model.RecipeApiModel
import hu.tamasknizner.cookbook.model.RecipeIngredientApiModel
import java.util.UUID

fun NewIngredientApiModel.toDomain() = NewIngredient(
    name = name,
    description = description,
)

fun NewRecipeIngredientApiModel.toDomain() = NewRecipeIngredient(
    ingredientId = ingredientId,
    quantity = quantity,
)

fun NewRecipeApiModel.toDomain() = NewRecipe(
    name = name,
    description = description,
    ingredients = ingredients.map { it.toDomain() },
)

fun Ingredient.toApiModel() = IngredientApiModel(
    id = id,
    name = name,
    description = description,
)

fun RecipeIngredient.toApiModel() = RecipeIngredientApiModel(
    ingredient = ingredient.toApiModel(),
    quantity = quantity.toString(),
)

fun Recipe.toApiModel() = RecipeApiModel(
    id = id,
    name = name,
    description = description,
    ingredients = ingredients.map { it.toApiModel() },
)
