package hu.tamasknizner.cookbook.backend.domain

import java.util.UUID

data class Ingredient(
    val id: UUID,
    val name: String,
    val description: String? = null,
)

data class NewIngredient(
    val name: String,
    val description: String? = null,
)

data class RecipeIngredient(
    val ingredient: Ingredient,
    val quantity: Int,
)

data class Recipe(
    val id: UUID,
    val name: String,
    val description: String? = null,
    val ingredients: List<RecipeIngredient>,
)

data class NewRecipeIngredient(
    val ingredientId: UUID,
    val quantity: Int,
)

data class NewRecipe(
    val name: String,
    val description: String? = null,
    val ingredients: List<NewRecipeIngredient>,
)
