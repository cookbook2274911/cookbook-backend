package hu.tamasknizner.cookbook.backend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CookbookBackendApplication

fun main(args: Array<String>) {
    runApplication<CookbookBackendApplication>(*args)
}
