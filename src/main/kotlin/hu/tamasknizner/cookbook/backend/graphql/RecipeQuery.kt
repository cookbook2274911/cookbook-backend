package hu.tamasknizner.cookbook.backend.graphql

import hu.tamasknizner.cookbook.backend.data.repository.RecipeRepository
import hu.tamasknizner.cookbook.backend.domain.toApiModel
import hu.tamasknizner.cookbook.model.AllRecipesApiModel
import hu.tamasknizner.cookbook.model.RecipeApiModel
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.web.server.ResponseStatusException
import java.util.UUID

@Controller
class RecipeQuery(
    private val recipeRepository: RecipeRepository
) {
    @QueryMapping
    fun getRecipes(): AllRecipesApiModel {
        return AllRecipesApiModel(
            recipes = recipeRepository.findAll().map { it.toApiModel() }
        )
    }

    @QueryMapping
    fun getRecipeById(@Argument id: UUID): RecipeApiModel {
        return recipeRepository.findById(id)?.toApiModel()
            ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Recipe not found with id: $id"
            )
    }
}