package hu.tamasknizner.cookbook.backend.graphql

import hu.tamasknizner.cookbook.backend.data.repository.IngredientRepository
import hu.tamasknizner.cookbook.backend.domain.toApiModel
import hu.tamasknizner.cookbook.model.AllIngredientsApiModel
import hu.tamasknizner.cookbook.model.IngredientApiModel
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.web.server.ResponseStatusException
import java.util.UUID

@Controller
class IngredientsQuery(
    private val ingredientRepository: IngredientRepository
) {
    @QueryMapping
    fun getIngredients(): AllIngredientsApiModel {
        return AllIngredientsApiModel(
            ingredients = ingredientRepository.findAll().map { it.toApiModel() }
        )
    }

    @QueryMapping
    fun getIngredientById(@Argument id: UUID): IngredientApiModel {
        return ingredientRepository.findById(id)?.toApiModel()
            ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Ingredient not found with id: $id"
            )
    }
}