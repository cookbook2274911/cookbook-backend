package hu.tamasknizner.cookbook.backend.controller

import hu.tamasknizner.cookbook.api.IngredientApi
import hu.tamasknizner.cookbook.backend.data.repository.IngredientRepository
import hu.tamasknizner.cookbook.backend.domain.toApiModel
import hu.tamasknizner.cookbook.backend.domain.toDomain
import hu.tamasknizner.cookbook.model.AllIngredientsApiModel
import hu.tamasknizner.cookbook.model.IngredientApiModel
import hu.tamasknizner.cookbook.model.NewIngredientApiModel
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.util.UUID

@RestController
class IngredientController(
    private val ingredientRepository: IngredientRepository,
) : IngredientApi {

    override fun getIngredientById(id: UUID): ResponseEntity<IngredientApiModel> {
        val ingredient = ingredientRepository.findById(id) ?: throw ResponseStatusException(
            HttpStatus.NOT_FOUND,
            "Ingredient not found with id: $id"
        )
        return ResponseEntity.ok(ingredient.toApiModel())
    }

    override fun getIngredients(): ResponseEntity<AllIngredientsApiModel> {
        return ingredientRepository.findAll()
            .map { it.toApiModel() }
            .let { ResponseEntity.ok(AllIngredientsApiModel(it)) }
    }

    override fun saveIngredient(newIngredientApiModel: NewIngredientApiModel): ResponseEntity<IngredientApiModel> {
        val newIngredient = newIngredientApiModel.toDomain()
        val savedIngredient = ingredientRepository.save(newIngredient)
        return ResponseEntity.ok(savedIngredient.toApiModel())
    }
}