package hu.tamasknizner.cookbook.backend.controller

import hu.tamasknizner.cookbook.api.RecipeApi
import hu.tamasknizner.cookbook.backend.data.repository.RecipeRepository
import hu.tamasknizner.cookbook.backend.domain.toApiModel
import hu.tamasknizner.cookbook.backend.domain.toDomain
import hu.tamasknizner.cookbook.model.AllRecipesApiModel
import hu.tamasknizner.cookbook.model.NewRecipeApiModel
import hu.tamasknizner.cookbook.model.RecipeApiModel
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.util.UUID

@RestController
class RecipeController(
    private val recipeRepository: RecipeRepository
) : RecipeApi {

    override fun getRecipeById(id: UUID): ResponseEntity<RecipeApiModel> {
        val recipe = recipeRepository.findById(id) ?: throw ResponseStatusException(
            HttpStatus.NOT_FOUND,
            "Recipe not found with id: $id"
        )
        return ResponseEntity.ok(recipe.toApiModel())
    }

    override fun getRecipes(): ResponseEntity<AllRecipesApiModel> {
        return ResponseEntity.ok(
            AllRecipesApiModel(
                recipes = recipeRepository.findAll().map { it.toApiModel() }
            )
        )
    }

    override fun saveRecipe(newRecipeApiModel: NewRecipeApiModel): ResponseEntity<RecipeApiModel> {
        val savedRecipe = recipeRepository.save(newRecipeApiModel.toDomain())
        return ResponseEntity.ok(savedRecipe.toApiModel())
    }
}