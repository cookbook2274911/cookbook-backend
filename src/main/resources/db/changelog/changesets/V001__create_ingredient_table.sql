create table ingredient
(
    id          uuid not null,
    name        text not null,
    description text,
    constraint ingredient__pk primary key (id)
)