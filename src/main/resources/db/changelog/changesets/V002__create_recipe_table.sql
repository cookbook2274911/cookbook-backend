create table recipe
(
    id          uuid not null,
    name        text not null,
    description text,
    constraint recipe__pk primary key (id)
);

create table recipe_ingredient
(
    id            uuid    not null,
    recipe_id     uuid    not null,
    ingredient_id uuid    not null,
    quantity      integer not null,
    constraint recipe_ingredient__pk primary key (id),
    constraint recipe_ingredient__recipe_id__fk foreign key (recipe_id) references recipe (id),
    constraint recipe_ingredient__ingredient_id__fk foreign key (ingredient_id) references ingredient (id)
)